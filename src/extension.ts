import * as vscode from 'vscode';
import { createTasksForHooks, createPreCommitTask, taskPrefix, TargetFiles } from './tasks';
import { PreCommitHookHandler } from './pre_commit_hooks';

// The handler for pre-commit hooks
let hookHandler: PreCommitHookHandler | undefined;

// A list of disposable events, to which this extension reacts.
let events: vscode.Disposable[] = [];

// Currently running tasks that were triggered by this extension.
let runningTaskExecutions: vscode.TaskExecution[] = [];

// Indicates that tasks are currently being set up.
let taskSetupActive: boolean = false;

// The number of completed tasks that were triggered by this extension.
let completedTaskCount: number = 0;

// The output channel of this extension.
export let outputChannel: vscode.OutputChannel;

/**
 * Executes a task and waits for its completion.
 *
 * @param task The task to execute in a blocking way.
 */
async function executeTaskBlocking(task: vscode.Task) {
	vscode.tasks.executeTask(task);

	return new Promise<void>(resolve => {
		let disposable = vscode.tasks.onDidEndTask(() => {
			disposable.dispose();
			resolve();
		});
	});
}

/**
 * Unregister all events, so that they do not trigger anymore.
 *
 * Used on deactivation of the extension.
 */
function unregisterEvents() {
	events.forEach(
		(disposable) => {
			disposable.dispose();
		}
	);
}

/**
 * Listens to ending tasks, and checks, if these tasks originate from this extension.
 *
 * Upon completion of such a task, increments the number of finished tasks.
 *
 * @param thisArg the argument that is passed on an event to the listener.
 */
function taskEndListener(thisArg: any) {
	let endedTaskExecution: vscode.TaskExecution = thisArg.execution;

	runningTaskExecutions.forEach((runningTaskExecution) => {
		if (endedTaskExecution === runningTaskExecution) {
			completedTaskCount++;
			outputChannel.appendLine(
				`${completedTaskCount}/${runningTaskExecutions.length} tasks ended ` +
				`[${runningTaskExecution.task.definition.type}].`
			);
		}
	});
}

/**
 * Checks, if all tasks that this extension launched, are complete.
 *
 * @returns true, if all tasks are finished, false, otherwise.
 */
function allTasksCompleted(): boolean {
	if (taskSetupActive) {
		return false;
	}

	if (completedTaskCount === runningTaskExecutions.length) {
		runningTaskExecutions = [];
		completedTaskCount = 0;
		return true;
	}
	else {
		return false;
	}
}

/**
 * Register all events, so that they trigger.
 *
 * Used on activation of the extension.
 */
function registerEvents() {
	events.push(
		vscode.tasks.onDidEndTask(taskEndListener)
	);

	events.push(
		vscode.workspace.onDidSaveTextDocument(
			() => {
				// Run only hooks that are specified in the configuration for running on save.
				run(vscode.workspace.getConfiguration('pre-commit-helper').get("runOnSave"), 'current file', true);
			}
		)
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runAllFiles",
			() => { run('all hooks', 'all files'); })
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runCurrentFile",
			() => { run('all hooks', 'current file'); })
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runChecksAllFiles",
			() => { run('checks', 'all files'); })
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runChecksCurrentFile",
			() => { run('checks', 'current file'); })
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runFixesAllFiles",
			() => { run('fixes', 'all files'); })
	);

	events.push(
		vscode.commands.registerCommand("pre-commit-helper.runFixesCurrentFile",
			() => { run('fixes', 'current file'); })
	);
}

/**
 * Checks, if the provided file is valid, and matches the defined regular expression.
 *
 * @param currentFilePath The path to the file to check against the matching criteria.
 * @returns true, if the file passes the checks, false otherwise.
 */
function fileMatchesCriteria(currentFilePath: string | undefined): boolean {
	// Regular expression to match against the file.
	let runOnSaveRegex: string | undefined =
		vscode.workspace.getConfiguration('pre-commit-helper').get("runOnSaveRegex");

	if (!currentFilePath) {
		outputChannel.appendLine(`No valid file path was found.`);
		return false;
	}

	if (currentFilePath.includes('.vscode/settings.json')) {
		outputChannel.appendLine(`The currently open 'settings.json' file is not checked by pre-commit hooks.`);
		return false;
	}

	if (runOnSaveRegex) {
		let regExp: RegExp | undefined;

		try {
			regExp = RegExp(runOnSaveRegex);
		} catch (error) {
			vscode.window.showWarningMessage(`Not a valid regular expression: ${runOnSaveRegex}.`);
		}

		if (!regExp?.test(currentFilePath)) {
			outputChannel.appendLine(`Open file does not match the defined selection criteria.`);
			return false;
		}
	}

	outputChannel.appendLine(`File '${currentFilePath}' matches criteria.`);

	// If all checks pass, this file can be used for hook execution.
	return true;
}

/**
 * The main entry point of this extension for running hooks.
 *
 * @param hooksToRun specifies, which types of hooks to run (fixes, checks, all hooks, or none).
 * @param targetFiles specifies the type of files to run hooks on (the current file, or all files).
 * @param checkFileMatching Decide, whether to check file types before running any hooks.
 */
async function run(
	hooksToRun: 'fixes' | 'checks' | 'all hooks' | 'none' | undefined,
	targetFiles: TargetFiles,
	checkFileMatching: boolean = false) {

	outputChannel.appendLine('##########');
	outputChannel.appendLine(`# Run '${hooksToRun}' on '${targetFiles}'.`);
	outputChannel.appendLine(`# File matching is '${checkFileMatching}'.`);
	outputChannel.appendLine('##########');

	if (!allTasksCompleted()) {
		// Do not allow to start new tasks before all old tasks are finished.
		outputChannel.appendLine(
			`Tasks are not completed yet (${runningTaskExecutions.length - completedTaskCount} pending).`
		);
		return;
	}

	// Indicate the beginning of task setup. This leads to allTasksCompleted() to return `false`.
	// Do not use an early return in this function, without setting this to `false` again.
	// Otherwise, this function will never trigger afterwards.
	taskSetupActive = true;

	const currentFilePath: string | undefined = vscode.window.activeTextEditor?.document.fileName;

	if (checkFileMatching && !fileMatchesCriteria(currentFilePath)) {
		outputChannel.appendLine(`Abort hook execution, as file(s) do not match the criteria.`);
		hooksToRun = undefined;
	}

	if (!hookHandler) {
		hookHandler = new PreCommitHookHandler();
	}

	if (hooksToRun && hookHandler.currentWorkspaceFolder) {
		outputChannel.appendLine(`Running hooks from workspace folder '${hookHandler.currentWorkspaceFolderPath}'.`);

		vscode.window.terminals.forEach(terminal => {
			if (terminal.creationOptions.name?.startsWith(taskPrefix)) {
				terminal.dispose();
			}
		});

		hookHandler.parseConfig();

		if (hookHandler.absoluteConfigPath && hookHandler.hooks.length > 0) {
			outputChannel.appendLine(`Detected ${hookHandler.hooks.length} hooks.`);

			if (['fixes', 'all hooks'].includes(hooksToRun) && hookHandler.fixHooks && currentFilePath) {
				let task = createPreCommitTask(
					hookHandler.absoluteConfigPath,
					targetFiles,
					hookHandler.notFixHooks,
					hookHandler.currentWorkspaceFolder
				);

				outputChannel.appendLine(`Add fix [${task.name}].`);
				await executeTaskBlocking(task);
			}

			if (['checks', 'all hooks'].includes(hooksToRun)) {
				let checkTasks = createTasksForHooks(
					hookHandler.checkHooks,
					hookHandler.absoluteConfigPath,
					targetFiles,
					hookHandler.currentWorkspaceFolder
				);

				for (let task of checkTasks) {
					outputChannel.appendLine(`Add check [${task.name}].`);
					runningTaskExecutions.push(await vscode.tasks.executeTask(task));
				}
			}
		}
		else {
			outputChannel.appendLine(`Hook execution aborted.`);
		}
	}

	// Task setup is finished.
	taskSetupActive = false;
}

/**
 * This method is called when the extension is activated.
 */
export function activate(_context: vscode.ExtensionContext) {
	const workspaceRoot = (vscode.workspace.workspaceFolders && (vscode.workspace.workspaceFolders.length > 0))
		? vscode.workspace.workspaceFolders[0].uri.fsPath : undefined;
	if (!workspaceRoot) {
		return;
	}

	outputChannel = vscode.window.createOutputChannel("pre-commit");

	registerEvents();
}

/**
 * This method is called when the extension is deactivated
 */
export function deactivate() {
	unregisterEvents();
}

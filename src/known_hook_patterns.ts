/**
 * This module includes patterns, against which hook IDs are checked.
 *
 * In order to determine, if hooks are checks or fixes, their IDs are compared with known `check` or `fix` patterns.
 */

/**
 * Patterns that are indicative of hooks that are fixes.
 */
export const knownFixPatterns: string[] = [
    'fix',
    'format',
    'sort'
];

/**
 * Patterns that are indicative of hooks that are checks.
 */
export const knownCheckPatterns: string[] = [
    // Special patterns (individual hooks)
    'mypy',
    'flake8',
    // General patterns
    'spell',
    'check',
    'lint',
    'style'
];

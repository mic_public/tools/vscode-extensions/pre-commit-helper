/**
 * This module creates vscode tasks from hook definitions.
 */
import * as vscode from 'vscode';
import { PreCommitHook } from './pre_commit_hooks';

export const taskPrefix: string = 'pre-commit';
const command = 'pre-commit';

/**
 * A type that specifies the file(s) to run hooks on.
 */
export type TargetFiles = 'current file' | 'all files';

/**
 * Default presentation options for all new tasks.
 * Tasks will never show their terminal on execution.
 */
const defaultPresentationOptions: vscode.TaskPresentationOptions = {
    clear: true,
    echo: false,
    focus: false,
    panel: vscode.TaskPanelKind.Dedicated,
    reveal: vscode.TaskRevealKind.Never,
    showReuseMessage: false
};

/**
 * Creates unique names by means of adding timestamps.
 *
 * @param name The non-unique name.
 * @returns The unique name, with added timestamp.
 */
function createUniqueName(name: string): string {
    return `${name} (${Date.now()})`;
}

/**
 * Creates a new task that executes all hooks that are defined in the pre-commit config.
 *
 * Exclude a list of hooks to skip. This is mainly used for 'fixes', as the generated task runs hooks in sequence.
 *
 * @param configFilePath The path to the pre-commit configuration file.
 * @param workspaceFolder The workspace folder to run the hook in.
 * @param targetFiles A selector, specifying on which files to run the hooks (current file, or all files).
 * @param skipHooks A list of hooks to skip during execution.
 * @returns The new task to execute.
 */
export function createPreCommitTask(
    configFilePath: string,
    targetFiles: TargetFiles,
    skipHooks: PreCommitHook[] = [],
    workspaceFolder: vscode.WorkspaceFolder): vscode.Task {

    let args = ['run', '-c', configFilePath];

    if (targetFiles === 'all files') {
        args.push('--all-files');
    }
    else {
        // The current file reference '${file}' is replaced by the vscode task runner on execution.
        args.push('--file');
        args.push('\${file}');
    }

    // Extract the IDs of all hooks to skip.
    let skipIds: string[] = [];
    for (let skipHook of skipHooks) {
        skipIds.push(skipHook.id);
    }

    // Arguments and environment variables for executing the pre-commit command
    let processExecutionOptions: vscode.ProcessExecutionOptions = {
        env: { // eslint-disable-next-line @typescript-eslint/naming-convention
            'SKIP': `${skipIds}`
        }
    };

    let processExecution = new vscode.ProcessExecution(
        command, // Command to execute
        args, // Arguments
        processExecutionOptions // Options for execution
    );

    let task = new vscode.Task(
        { type: createUniqueName(taskPrefix) },
        workspaceFolder, // Scope
        `${taskPrefix}: All hooks (skipping ${skipHooks.length})`, // Name
        taskPrefix, // Source
        processExecution
        // No problem matchers are used for sequential execution. They would possibly conflict.
    );

    task.presentationOptions = defaultPresentationOptions;

    return task;
}

/**
 * Create a task for a single hook definition.
 *
 * @param hook The hook to create a task for.
 * @param configFilePath The path to the pre-commit configuration file.
 * @param targetFiles A selector, specifying on which files to run the hooks (current file, or all files).
 * @param workspaceFolder The workspace folder to run the hook in.
 * @returns The new task to execute.
 */
export function createPreCommitTaskForSingleHook(
    hook: PreCommitHook,
    configFilePath: string,
    targetFiles: TargetFiles,
    workspaceFolder: vscode.WorkspaceFolder): vscode.Task {
    let args = ['run', hook.id, '-c', configFilePath];

    if (targetFiles === 'all files') {
        args.push('--all-files');
    }
    else {
        // The current file reference '${file}' is replaced by the vscode task runner on execution.
        args.push('--file');
        args.push('\${file}');
    }

    let problemMatchers: string[] | [] = [];
    if (hook.isCheck) {
        problemMatchers = [`\$${hook.id}`];
    }

    let processExecution = new vscode.ProcessExecution(
        command, // Command to execute
        args, // Arguments
    );

    let task = new vscode.Task(
        { type: createUniqueName(`${taskPrefix}: ${hook.id}`) },
        workspaceFolder, // Scope
        `${taskPrefix}: ${hook.id}`, // Name, same as type
        taskPrefix, // Source
        processExecution,
        problemMatchers
    );

    task.presentationOptions = defaultPresentationOptions;
    hook.task = task;

    return task;
}

/**
 * Creates individual tasks for pre-commit hooks, that can be executed concurrently.
 *
 * If an empty list of hooks is provided, an empty list of tasks is returned.
 *
 * @param hooks A list of hooks to create tasks for.
 * @param configFilePath The path to the pre-commit configuration file.
 * @param workspaceFolder The working directory to run the hooks from.
 * @param targetFiles A selector, specifying on which files to run the hooks (current file, or all files).
 */
export function createTasksForHooks(
    hooks: PreCommitHook[] | undefined,
    configFilePath: string,
    targetFiles: TargetFiles,
    workspaceFolder: vscode.WorkspaceFolder
): vscode.Task[] {

    if (!hooks) {
        return [];
    }

    let tasks: vscode.Task[] = [];

    for (let hook of hooks) {
        tasks.push(createPreCommitTaskForSingleHook(hook, configFilePath, targetFiles, workspaceFolder));
    }

    return tasks;
}
